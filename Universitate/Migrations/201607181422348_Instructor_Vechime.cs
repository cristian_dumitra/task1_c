namespace Universitate.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Instructor_Vechime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Instructor", "Vechime", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Instructor", "Vechime");
        }
    }
}
