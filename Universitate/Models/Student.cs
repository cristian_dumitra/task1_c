﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Universitate.Models
{
    public class Student
    {
        public int ID { get; set; }

        [StringLength(50)]
        [Required]
        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [Required]
        [DisplayName("First Name")]
        [Column("FirstName")]
        [StringLength(50, ErrorMessage = "Eroare", MinimumLength = 3)]
        public string FirstMidName { get; set; }


        [DisplayName("Enrollment Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? EnrollmentDate { get; set; } // zz-ll-yyyyy hh:mm
        
        [DisplayName("Full Name")]
        public string FullName
        {
            get { return LastName + ", " + FirstMidName; }
        }


        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }
}