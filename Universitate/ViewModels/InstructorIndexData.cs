﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Universitate.Models;

namespace Universitate.ViewModels
{
    public class InstructorIndexData
    {

        public virtual IEnumerable<Course> Courses { get; set; }

        public virtual IEnumerable<Enrollment> Enrollments { get; set; }

        public virtual IEnumerable<Instructor> Instructors { get; set; }


    }
}