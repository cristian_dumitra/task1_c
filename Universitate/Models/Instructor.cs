﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Universitate.Models
{
    public class Instructor
    {

        public int ID { get; set; }

        [DisplayName("Last Name")]
        [StringLength(50, MinimumLength = 1)]
        public string LastName { get; set; }

        [DisplayName("First Name")]
        [Column("FirstName")]
        [StringLength(50, MinimumLength = 1)]
        public string FirstMidName { get; set; }

        [DisplayName("Hire Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.Date)]
        public DateTime HireDate { get; set; }

        [DisplayName("Full Name")]
        public string FullName {

            get{ return LastName + ", " + FirstMidName; }

        }


        public int Vechime { get; set; }

        public virtual ICollection<Course> Courses { get; set; }

        public virtual  OfficeAssignment OfficeAssignment { get; set; }



    }
}